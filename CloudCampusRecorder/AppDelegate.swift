//
//  AppDelegate.swift
//  CloudCampusRecorder
//

import Cocoa
import WebKit
@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
  
  @IBOutlet weak var webFrameView: NSView!
  @IBOutlet weak var startButton: NSButton!
  @IBOutlet weak var window: NSWindow!
  @IBOutlet weak var label: NSTextField!
  
  @IBOutlet var tableController: TableController!
  @IBOutlet var webController: WebController!
  var timer: Timer? = nil
  var uiTimer: Timer!
  
  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
    webController.load(inView: webFrameView)
    uiTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI(_:)), userInfo: nil, repeats: true)
  }
  
  @objc func updateUI(_ sender: Any) {
    if timer == nil {
      label?.stringValue = ""
      return
    }
    
    let secondsToGo = (timer!.fireDate.timeIntervalSinceReferenceDate - Date().timeIntervalSinceReferenceDate )
    let minutes: Int = Int(secondsToGo/60)
    
    if minutes > 0 {
      let text = "この章はあと\(String(minutes))分"
      label?.stringValue = text
    }else {
      let sec: Int = Int(secondsToGo)
      let text = "この章はあと\(String(sec))秒"
      label?.stringValue = text
    }
  }

  @IBAction func start(_ sender: Any) {
    guard tableController.canStart == true else { return }
    if timer != nil {
      stop()
      return
    }
    
    webController.isAutopilot = true
    setScriptForStopping(duration: tableController.totalDuration)
    next()
  }
  
  
  func stop() {
    startButton.title = "Start"
    timer?.invalidate()
    timer = nil
    
    let asfile = Bundle.main.url(forResource: "AppleScriptStopRecording", withExtension: "applescript")!
    let asString = try! String(contentsOf: asfile)
    let script = NSAppleScript(source: asString)
    
    script?.executeAndReturnError(nil)
    webController.isAutopilot = false
  }
  
  @objc func next() {
    guard let obj = (tableController.arrangedObjects as? [Entry])?.first  else {
      stop()
      return
    }
    
    startButton.title = "Stop"
    timer?.invalidate()
    timer = Timer.scheduledTimer(timeInterval: Double(obj.duration) * 60.0, target: self, selector: #selector(next), userInfo: nil, repeats: false)
    
    webController.load(obj.url)
    tableController.remove(atArrangedObjectIndex: 0)
  }
  
  
  func setScriptForStopping(duration: Int) {
    guard let asfile = Bundle.main.url(forResource: "AppleScript", withExtension: "applescript") else {
    print("*************")
      return
    }
    
    let asString = try! String(contentsOf: asfile)
    
    let script = NSAppleScript(source: asString)
    webController.scriptOnFinishLoadingSite = script
  }
}

class WebController: NSObject, WKNavigationDelegate, WKUIDelegate {
  weak var webView: WKWebView!
  var isAutopilot: Bool = false
  var scriptOnFinishLoadingSite: NSAppleScript? = nil
  
  @IBAction func goBack(_ sender: Any) {
    webView.goBack()
  }
  @IBAction func goForward(_ sender: Any) {
    webView.goForward()
  }
  @IBAction func reload(_ sender: Any) {
    webView.reload()
  }
  
  func load(inView view: NSView) {
    let preferences = WKPreferences()
    preferences.plugInsEnabled = true
    preferences.javaEnabled = true
    preferences.javaScriptEnabled = true
    
    let config = WKWebViewConfiguration()
    config.preferences = preferences
    let webView = WKWebView(frame: view.bounds, configuration: config)
    webView.autoresizingMask = [.width, .height]
    view.addSubview(webView)
    self.webView = webView
    
    let address = "https://lms.cyber-u.ac.jp/moodle/my/"
    let url = URL(string: address)!
    
    webView.navigationDelegate = self
    webView.uiDelegate = self
    
    load(url)
    
    //    let sampleUrl = Bundle.main.url(forResource: "Sample", withExtension: "html")!
    //    let request = URLRequest(url: sampleUrl, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 30.0)
    //    let navigation = webView.load(request)
    
  }
  
  func load(_ url: URL) {
    let request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30.0)
    let _ = webView.load(request)
  }
  
  //https://lms.cyber-u.ac.jp/moodle/mod/scorm/view
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    let url = webView.url
    
    if isAutopilot && url?.absoluteString.hasPrefix("https://lms.cyber-u.ac.jp/moodle/mod/scorm/view") == true {
      // document.forms["myForm"].submit();
      // <form id="theform" method="post" action="https://lms.cyber-u.ac.jp/moodle/mod/scorm/player.php">
      
      let script = "document.forms[\"theform\"].submit();"
      webView.evaluateJavaScript(script, completionHandler: { (obj, error) in
      })
    }else {
      
      if scriptOnFinishLoadingSite != nil {
        var dict: NSDictionary? = NSDictionary()
        scriptOnFinishLoadingSite?.executeAndReturnError(&dict)
        scriptOnFinishLoadingSite = nil
      }
    }
  }
  
  func webView(_ sender: WebView!, contextMenuItemsForElement element: [AnyHashable : Any]!, defaultMenuItems: [Any]!) -> [Any]! {
    return []
  }
}

class MyTableView: NSTableView {
  
  func canAccept(_ pb: NSPasteboard) -> Bool {
    guard let urlString = pb.string(forType: kUTTypeURL as NSPasteboard.PasteboardType) else { return false }
    if urlString.hasPrefix("https://lms.cyber-u.ac.jp/moodle/mod/scorm/view") == true {
      return true
    }
    return false
  }
  
  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    let pb = sender.draggingPasteboard()
    if canAccept(pb) {
      return NSDragOperation.copy
    }
    return NSDragOperation.generic
  }
  
  override func draggingUpdated(_ sender: NSDraggingInfo) -> NSDragOperation {
    let pb = sender.draggingPasteboard()
    if canAccept(pb) {
      return NSDragOperation.copy
    }
    return NSDragOperation.generic
  }
  
  override func draggingEnded(_ sender: NSDraggingInfo){
  }
  
  override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
    let pb = sender.draggingPasteboard()
    return canAccept(pb)
  }
  
  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool{
    let pb = sender.draggingPasteboard()
    guard let urlString = pb.string(forType: kUTTypeURL as NSPasteboard.PasteboardType) else { return false }
    
    let urlName = pb.string(forType: ("public.url-name" as NSString) as NSPasteboard.PasteboardType) ?? "サイト"
    
    if urlString.hasPrefix("https://lms.cyber-u.ac.jp/moodle/mod/scorm/view") == true {
      
      let entry = Entry()
      entry.title = urlName
      entry.url = URL(string: urlString)
      entry.duration = 25
      (self.delegate as? TableController)?.addObject(entry)
      return true
    }
    
    return false
  }
  
  override func concludeDragOperation(_ sender: NSDraggingInfo?) {
    sender?.animatesToDestination = true
  }
}


class Entry: NSObject {
  @objc dynamic var title: String!
  @objc dynamic var url: URL!
  @objc dynamic var duration: Int = 25
}

class TableController: NSArrayController, NSTableViewDataSource, NSTableViewDelegate  {
  
  @objc dynamic var canStart: Bool { return (arrangedObjects as! [Entry]).count > 0 }
  var totalDuration: Int {
    let entries = arrangedObjects as! [Entry]
    return entries.reduce(0) { $0 + $1.duration }
  }
  
  override func addObject(_ object: Any) {
    self.willChangeValue(forKey: "canStart")
    super.addObject(object)
    self.didChangeValue(forKey: "canStart")
  }
  
  @IBOutlet weak var tableView: NSTableView! {
    didSet {
      tableView.delegate = self
      
      if #available(OSX 10.13, *) {
        tableView.registerForDraggedTypes([NSPasteboard.PasteboardType.URL])
      } else {
        tableView.registerForDraggedTypes([kUTTypeURL as NSPasteboard.PasteboardType])
      }
    }
  }
  
  func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
    return true
  }
}

